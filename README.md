Earth - the Unity-project for Android

The project was created using Vuforia Engine and it is suitable for android devices with version 5 and higher.
For the application, you need to prepare a field(surface.jpg) and install apk file.
After installation, you need to start the application, give access to the camera and point it at the area.
You will see an object(earth). You can interact with it with the buttons. 
It is important that the sound on the phone is turned on, as the application uses music.
It is also possible to run the project from the pc, where you need to interact with the aplication with a mouse(But the field is still necessarily need).

Thank you for the using my application!
I hope you like it =)
		