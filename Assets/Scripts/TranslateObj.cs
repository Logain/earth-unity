﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateObj : MonoBehaviour
{
    public GameObject earth;

    public float rotateSpeed = 30f;
    
    bool rotateStatus = false;
    bool sUp = false;
    bool sDown = false;
    bool mLeft = false;
    bool mRight = false;
    bool mForward = false;
    bool mBack = false;
    bool mUp = false;
    bool mDown = false;

    void Update()
    {
        if (rotateStatus)
        {
            earth.transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime);
        }
        if (sUp)
        {
            earth.transform.localScale += new Vector3(0.005f, 0.005f, 0.005f);
        }
        if (sDown)
        {
            earth.transform.localScale += new Vector3(-0.005f, -0.005f, -0.005f);
        }
        if (mLeft)
        {
            earth.transform.Translate(-0.25f * Time.deltaTime, 0, 0);
        }
        if (mRight)
        {
            earth.transform.Translate(0.25f * Time.deltaTime, 0, 0);
        }
        if (mForward)
        {
            earth.transform.Translate(0, 0, 0.25f * Time.deltaTime);
        }
        if (mBack)
        {
            earth.transform.Translate(0, 0, -0.25f * Time.deltaTime);
        }
        if (mUp)
        {
            earth.transform.Translate(0, 0.25f * Time.deltaTime, 0);
        }
        if (mDown)
        {
            earth.transform.Translate(0, -0.25f * Time.deltaTime, 0);
        }
    }
    
    public void RotateObj()
    {
        if (rotateStatus == false)
        {
            rotateStatus = true;
        }
        else
        {
            rotateStatus = false;
        }
    }

    public void ScaleUp()
    {
        sUp = true;
    }

    public void StopScaleUp()
    {
        sUp = false;
    }

    public void ScaleDown()
    {
        sDown = true;
    }

    public void StopScaleDown()
    {
        sDown = false;
    }
    public void MovingLeft()
    {
        mLeft = true;
    }

    public void StopMovingLeft()
    {
        mLeft = false;
    }

    public void MovingRight()
    {
        mRight = true;
    }

    public void StopMovingRight()
    {
        mRight = false;
    }

    public void MovingForward()
    {
        mForward = true;
    }

    public void StopMovingForward()
    {
        mForward = false;
    }

    public void MovingBack()
    {
        mBack = true;
    }

    public void StopMovingBack()
    {
        mBack = false;
    }

    public void MovingUp()
    {
        mUp = true;
    }

    public void StopMovingUp()
    {
        mUp = false;
    }

    public void MovingDown()
    {
        mDown = true;
    }

    public void StopMovingDown()
    {
        mDown = false;
    }

}
